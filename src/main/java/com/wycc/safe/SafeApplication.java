package com.wycc.safe;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@MapperScan("com.wycc.safe.mapper")
public class SafeApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SafeApplication.class, args);
        System.out.println(1);
    }

}
