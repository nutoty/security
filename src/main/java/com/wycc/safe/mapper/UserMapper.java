package com.wycc.safe.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wycc.safe.entity.User;

/**
 * @Author Wu Yong
 * @Date 2022/5/3 13:41
 * @Description
 */
public interface UserMapper extends BaseMapper<User> {
}
