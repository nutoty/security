package com.wycc.safe.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wycc.safe.entity.Menu;

import java.util.List;

/**
 * @Author Wu Yong
 * @Date 2022/5/3 22:25
 * @Description
 */
public interface MenuMapper extends BaseMapper<Menu> {

    List<String> selectPermsByUserId(Long id);
}
