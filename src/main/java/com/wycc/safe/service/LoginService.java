package com.wycc.safe.service;

import com.wycc.safe.entity.User;
import com.wycc.safe.vo.ResponseResult;

/**
 * @Author Wu Yong
 * @Date 2022/5/3 14:10
 * @Description
 */
public interface LoginService {
    ResponseResult login(User user);

    ResponseResult logout();
}
