package com.wycc.safe.controller;

import com.wycc.safe.entity.User;
import com.wycc.safe.service.LoginService;
import com.wycc.safe.vo.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @Author Wu Yong
 * @Date 2022/5/3 14:06
 * @Description
 */
@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseResult login(@RequestBody User user){
        return loginService.login(user);
    }

    @GetMapping("/logout")
    public ResponseResult logout(){
        return loginService.logout();
    }
}
