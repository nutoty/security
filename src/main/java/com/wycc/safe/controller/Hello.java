package com.wycc.safe.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Wu Yong
 * @Date 2022/5/3 13:02
 * @Description
 */
@RestController
public class Hello {

    @GetMapping("/hello")
//    @PreAuthorize("hasAuthority('system:dept:list1')")
    @PreAuthorize("@WYExpressionRoot.hasAuthority('system:dept:list')")
    public String hello(){
        return "hello";
    }
}
