package com.wycc.safe.exception;

import com.alibaba.fastjson.JSON;
import com.wycc.safe.utils.WebUtils;
import com.wycc.safe.vo.ResponseResult;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author Wu Yong
 * @Date 2022/5/4 11:15
 * @Description
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        ResponseResult result = new ResponseResult(HttpStatus.FORBIDDEN.value(), "您的权限不足");
        String response = JSON.toJSONString(result);
        WebUtils.renderString(httpServletResponse, response);
    }
}
