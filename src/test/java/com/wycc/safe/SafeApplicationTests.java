package com.wycc.safe;

import com.wycc.safe.entity.User;
import com.wycc.safe.mapper.MenuMapper;
import com.wycc.safe.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@SpringBootTest
class SafeApplicationTests {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private MenuMapper menuMapper;

    @Test
    void contextLoads() {
    }

    @Test
    public void testUserMapper(){
        List<User> users = userMapper.selectList(null);
        System.out.println(users);
    }

    @Test
    public void testSelectPermsByUserId(){
        List<String> perms = menuMapper.selectPermsByUserId(2L);
        System.out.println(perms);
    }

    @Test
    public void test1(){
        List<Integer> list = new ArrayList<>(Arrays.asList(1,2,6));
        list.stream().sorted((o1, o2) -> o2 - o1).forEach(s -> System.out.println(s));
    }


}
